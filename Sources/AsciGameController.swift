//
//  AsciBoardController.swift
//  AsciConnect4
//
//  Created by Thomas Kausch on 15.01.17.
//
//

import SwiftBoardGameLibrary


public class AsciGameController {
    
    let DefaultSearchLevel = 8
    
    var board: ConnectBoard = ConnectBoard()
    let searchEngine: AlphaBetaSearchEngine<ConnectBoard> = AlphaBetaSearchEngine()
    
    public init() {
    }
    
    func playWhiteMove() {
        print("\n\(board.description)")
        var move : ConnectMove?
        while move == nil {
            print("Please enter column number (1-7):")
            let input = readLine(strippingNewline: true)
            if let colString = input {
                let column = Int(colString)
                if let col = column {
                    let nextMove = ConnectMove(player: .white, column: col)
                    if board.validMoves.contains(nextMove) {
                        move = nextMove
                    } else {
                        print("ERROR: This is not a valid column number")
                    }
                } else {
                    print("ERROR: Not a number")
                }
            }
        }
        board.execute(move: move!)
        print("\n\(board.description)")
    }
    
    func playBestBlackMove() {
        print("Computer is moving. Please Wait!")
        let (bestBlackMove, _) = searchEngine.bestMove(board: &board, searchDepth: DefaultSearchLevel)
        board.execute(move: bestBlackMove!)
    }
    
    func showWinner() {
        print("\n\(board.description)")
        if let winner = board.winner {
            switch winner {
            case .black:
                print("Sorry you have lost.")
            case .white:
                print("Congrat YOU are the winner!");
            }
        } else {
            print("Game is draw!")
        }
    }
    
    public func play() {
        while !board.isEndPosition {
            playWhiteMove()
            if !board.isEndPosition {
                playBestBlackMove()
            }
        }
        showWinner()
    }
    
}
