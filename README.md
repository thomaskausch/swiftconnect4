# SwiftConnect4 #
This is a sample project about how to use `SwiftGameBoardLibrary`. 

## Building ##
Build the  Project with the following commands.

```Bash
thomaskausch@macmini:$ swift build
Cloning https://bitbucket.org/thomaskausch/swiftboardgamelibrary.git
HEAD is now at 4702628 Bugfix in testing from connect board
Resolved version: 1.1.1
Compile Swift Module 'SwiftBoardGameLibrary' (7 sources)
Compile Swift Module 'SwiftConnect4' (2 sources)
Linking ./.build/debug/SwiftConnect4
```

Running this command line fetches the necessary dependencies before creating the XCoce project. Note: When you build with Xcode, it does not use SwiftPM to build your project, but instead will use the Xcode native build system.

## Running ##
After you have built you can run connect 4 in a terminal. The board is printed in an ASCI representation. `X` represents a white and `0` black field. An empty field is represented with a `-`. 

```Bash
thomaskausch@macmini$ .build/debug/SwiftConnect4

- - - - - - - 
- - - - - - - 
- - - - - - - 
- - - - - - - 
- - - - - - - 
- - - - - - - 

Please enter column number (1-7):

```


## Developing in XCode
While you can easily develp entierly on the command line using `swift`and your favorite text editor, most developers want to use XCode for their Swift development. In order to use XCode, a project must be created from your `Package.swift`. The following command will create your project:

```Bash
$ swift package generate-xcodeproj
$ open SwiftConnect4.xcodeproj/
```

If you add or remove a dependency to your `Package.swift` file, it will be necessary for you to recreate the Xcode project again. Note that any changes made to your project will be lost when a new project file is generated over the old one.