import PackageDescription

let package = Package(
    name: "SwiftConnect4",
    dependencies: [
       .Package(url: "https://bitbucket.org/thomaskausch/swiftboardgamelibrary.git", 
       majorVersion: 1, minor: 1)
    ]
)    
